using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceName
{
  noResource = 0,  

  //money 
  gold = 1,
  diamond = 2,

  //material level 1
  logwood = 1001,

  //material level 2
  blank = 2001,
}
