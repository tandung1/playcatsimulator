using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Resource
{
    public ResourceName name;
    public int number;
}
