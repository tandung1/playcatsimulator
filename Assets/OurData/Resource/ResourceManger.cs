using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManger : MonoBehaviour
{
    public static ResourceManger instance; 

    [SerializeField] protected List<Resource> resources;

    protected void Awake()
    {
        if (ResourceManger.instance != null) Debug.LogError("sai me roi");
        ResourceManger.instance = this;
    }

    public virtual Resource AddResource(ResourceName resourceName, int number)
    {
        Debug.Log("add" + resourceName + " " + number);
        Resource res = this.GetResByName(resourceName);
  
        res.number += number;
        return res;
    }

    public virtual Resource GetResByName(ResourceName resourceName)
    {
        Resource res = this.resources.Find((x) => x.name == resourceName);

        if (res == null)
        {
            res = new Resource();
            res.name = resourceName;
            res.number = 0;

            this.resources.Add(res);
        }
        return res;
    }
}
